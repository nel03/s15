// alert("Hello World");
// 	console.log("Hello World");

let activity1 = "Activity 1";
	console.log(activity1);

let firstName = "Ronel";
	console.log("First Name: " + firstName);

let lastName = "Tayawa";
	console.log("Last Name: " + lastName);

let age = 30;
	console.log("Age: " + age);

const hobbies = ["Running", "Hiking", "Traveling"]
	console.log("Hobbies:")
	console.log(hobbies);

const workAddress = {
	houseNumber: 243,
	street: "275 E.Rodriguez Brgy Kalusugan",
	city: "Quezon City",
	state: "Metro Manila"
}
	console.log("Work Address:")
	console.log(workAddress)

/*Debugging Practice*/

let activity2 = "Activity 2";
	console.log(activity2);

let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint","Nick"]
	console.log("My Friends are: ");
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation = "Arctic Ocean";
	{
	let lastLocation = "Atlantic Ocean";
	}
	console.log("I was found frozen in: " + lastLocation);