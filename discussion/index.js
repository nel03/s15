// > single line comment
/* this is a multiline comment
	shortcut key: ctrl + shift + /
*/

// alert("Hello World");
// console.log("Hello World");
// console.log(3+2);
// variables
// let name = 'Nel';
// console.log(name);

	let name;
// 'nel' is called string
	name = "Nel"; /*asigned*/
	name = "Ronel";/*re-asigned*/
// 22 = called number
	let age = 22;

	console.log(name);

	const boilingPoin = 100;
	boilingPoint = 200;

	console.log(boilingPoint);
	console.log(age+'3');
	console.log(name+3);

// Boollean = true or false
	let isAlive = true;
	console.log(isAlive);

// Arrays uses []/ is used to group items together
	let grades = [98, 96, 95, 90];

	let grade1 = 98;
	let grade2 = 96;
	let grade3 = 95;
	let grade4 = 90;

	console.log(grades[5]);

	let movie = "jaws";
// console.log(song);
	let isp;
	console.log(isp);
// this is undefined because the variable has no value
// to assign a value use the = sign

// null vs undefined ?
/*undefined
	> Represents the state of a variable that has been declared but without an assigned value	
*/

/* null
	> used intentionally to express the absence of a value in a declared initialized variabl
*/

	let spouse = null;
	console.log(spouse);

/* Objects
	> special kind of data type that is used to mimic real world objects/items
		syntax:
			let objectName = {
				property: keyValue, 
				property2: keyVlaue2,
				property3: keyVlaue3,
				property4: keyVlaue4,					
				}
*/

	let myGrades = {
		fristGrading: 98,
		SecondGrading: 92,
		thirdGrading: 90,
		fourthgrading:94.6,
		}

	console.log(myGrades);

	let person = {
		fullName: "Alonzo Cruz",
		age: 25,
		isMarried: false,
		contact:['0091321313', '092321564'],
		address: {
		houseNumber: 345,
		city: 'Manila'
			}
		}

	console.log(person);